Indefinetly unfinished mlog (Mindustry logic) compiler from custom language with
syntax almost, but not entirely unlike Rust.

You should use one of the other compilers, or write LLVM backend. If you do end
up using this, feel free to open issues, or PRs as it might just motivate me to
work on this.

This repo is very tentative, so check `test_code` to see the language syntax for
now. If the syntax is not clear and you still wish to use this, open an issue, and I'll try to come up with
specification/tutorial. The only intrinsic is the `asm()` function, which allows
to declare input/output variables and insert arbitrary mlog instructions through
the special syntax.

This is the very first iteration that even produces code, so the modules are
misnamed, the generation phases are not well separated and the code (input,
output and the compiler itself) is very hard and inefficient to use. If the
architecture is not clear and you wish to contribute, open an issue, and I'll
try to come up with some documentation and contribution guidelines.
