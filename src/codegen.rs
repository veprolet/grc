use crate::parse;
use std::collections::HashMap;

#[derive(Default, Debug, PartialEq, Clone)]
pub struct CodegenState<'a> {
    pub functions: HashMap<String, Function<'a>>, // (name, arity), number of calls
}

impl CodegenState<'_> {
    pub fn new() -> Self {
        CodegenState::default()
    }
}

#[derive(Debug, PartialEq, Clone)]
pub struct Function<'a> {
    pub return_type: parse::Type,
    //pub calls: usize,
    pub declaration: &'a parse::Item<'a>,
}

pub fn codegen_module<'a>(state: &mut CodegenState<'a>, code: &'a parse::Module<'a>) {
    for item in &code.items {
        codegen_item(state, item);
    }
}
fn codegen_item<'a>(state: &mut CodegenState<'a>, item: &'a parse::Item<'a>) {
    match item {
        parse::Item::FnDeclaration {
            name,
            params: _,
            return_type,
            body: _,
        } => {
            let prev = state.functions.insert(
                (*name).to_owned(),
                Function {
                    return_type: return_type.clone().unwrap_or(parse::Type::Tuple(vec![])),
                    //calls: 0,
                    declaration: item
                },
            );
            if prev.is_some() {
                panic!("Double declaration of fn {}", name);
            }
        }
    }
}
