use std::collections::hash_map;
use std::collections::hash_map::Entry;
use std::collections::HashMap;
use std::fmt::Display;
use std::iter::once;
use std::rc::Rc;

use crate::codegen;
use crate::parse;

// construct the call graph
//
// block:
//  in edge count
//  jump - delayed
//
//
//  edge -> jump (+ save jump location?)
//
//  id, instance number
//
// 1 in/out -> inline
//
//
// recursion
// return stack frames
// only one out edge -> possible inline

#[derive(Debug, PartialEq, Clone)]
struct EmitState<'a> {
    var_scope: Vec<HashMap<String, usize>>,
    last_var: usize,
    functions: HashMap<String, Function>,
    codegen: &'a codegen::CodegenState<'a>,
}

impl<'a> EmitState<'a> {
    fn new(codegen: &'a codegen::CodegenState) -> EmitState<'a> {
        EmitState {
            var_scope: Vec::new(),
            last_var: 0,
            functions: HashMap::new(),
            codegen,
        }
    }

    fn next_var(&mut self) -> usize {
        let ret = self.last_var;
        self.last_var += 1;
        ret
    }
}

#[derive(Debug, PartialEq, Clone)]
struct Function {
    called: bool,
    block: Rc<Block>,
    ref_count: usize,
}

// Pseudoinstruction with bounded maximal width in the terms of occupied address space if
// applicable
#[derive(Debug, PartialEq, Clone)]
enum Instruction {
    Set {
        target: Variable,
        value: Value,
    },
    // TODO beter types to represent only valid operations
    Operation {
        target: Variable,
        operation: Operation,
        lhs: Value,
        rhs: Value,
    },
    Jump {
        target: Variable,
        condition: JumpCondition,
        lhs: Value,
        rhs: Value,
    },
    Raw(String)
}

impl Display for Instruction {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Instruction::Set { target, value } => writeln!(f, "set {} {}", target, value),
            Instruction::Operation { target, operation, lhs, rhs } => writeln!(f, "op {} {} {} {}", operation, target, lhs, rhs),
            Instruction::Jump { target, condition, lhs, rhs } => todo!(),
            Instruction::Raw(instruction) => writeln!(f, "{}", instruction),
        }
    }
}

#[derive(Debug, PartialEq, Clone)]
enum Value {
    Number(f64),
    Variable(Variable),
}

#[derive(Debug, PartialEq, Clone)]
// TODO change ret_vals and such to be Variable
// TODO allow descriptive tag (e.g. var:123 -> var:ret_val_123)
struct Variable(usize);

impl Display for Variable {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "var:{}", self.0)
    }
}

impl Display for Value {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Value::Number(number) => write!(f, "{}", number),
            Value::Variable(variable) => write!(f, "{}", variable),
        }
    }
}

#[derive(Debug, PartialEq, Clone)]
enum JumpCondition {
    Equal,
    NotEqual,
    Less,
    LessEqual,
    Greater,
    GreaterEqual,
    TypeEqual,
    Always,
}

#[derive(Debug, PartialEq, Clone)]
enum Operation {
    Add,
    Sub,
    Mul,
    Div,
    IntDiv,
    Mod,
    Pow,
    LogicNot,
    LogicAnd,
    BinaryAnd,
    BinaryOr,
    BinaryXor,
    Equal,
    NotEqual,
    Less,
    LessEqual,
    Greater,
    GreaterEqual,
    TypeEqual,
    ShiftLeft,
    ShiftRight,
    Max,
    Min,
    Angle,
    Len,
    Noise,
    Abs,
    Log,
    Log10,
    Sin,
    Cos,
    Tan,
    Floor,
    Ceil,
    Sqrt,
    Rand,
    Not,//Flip,
}

impl Display for Operation {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Operation::Add => write!(f, "add"),
            Operation::Sub => write!(f, "sub"),
            Operation::Mul => write!(f, "mul"),
            Operation::Div => write!(f, "div"),
            _ => todo!()
        }
    }
}

impl Operation {
    fn from_operator(op: &str) -> Self {
        match op {
            "+" => Self::Add,
            "-" => Self::Sub,
            "*" => Self::Mul,
            "/" => Self::Div,
            "%" => Self::Mod,
            "&" => Self::BinaryAnd,
            "|" => Self::BinaryOr,
            "^" => Self::BinaryXor,
            "&&" => Self::LogicAnd,
            "||" => Self::BinaryOr,
            _ => {
                panic!("Unknown operator \"{}\"", op)
            }
        }
    }
}

// Block contains no jumps
// Two blocks of code are always interchangeable
#[derive(Debug, PartialEq, Clone)]
struct Block {
    code: Vec<Instruction>,
}

impl Block {
    fn empty() -> Self {
        Self { code: Vec::new() }
    }
    fn new(code: Vec<Instruction>) -> Self {
        Self { code }
    }
}

// Multiple nodes in the call graph can point to the same block, but differ in the in/out edges
#[derive(Debug, PartialEq, Clone)]
pub struct Node {
    code: Block,
    // The first children is the implicit jump to the next instruction if appropriate for the given
    // last instruction
    // Children apply to the last instruction of the block
    children: Vec<Node>,
}

impl Node {
    // TODO better naming or interface
    fn new(code: Vec<Instruction>) -> Self {
        Self {
            code: Block::new(code),
            children: Vec::new(),
        }
    }
    fn empty() -> Self {
        Self {
            code: Block::empty(),
            children: Vec::new(),
        }
    }
    // TODO separate type for divergent nodes/instructions?
    // TODO allow appending at the end of a chain?
    fn append(&mut self, mut node: Node) -> &mut Self {
        assert!(self.children.is_empty(), "Can't append to node that has children");
        self.code.code.append(&mut node.code.code);
        self.children = node.children;
        self
    }
}

impl Display for Node {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        for instruction in &self.code.code {
            instruction.fmt(f)?;
        }

        if !self.children.is_empty() {
            assert_eq!(self.children.len(), 1, "Attempted to serialize nonlinear graph {:#?}", self);
            self.children.get(0).expect("Linearity of graph was not ensured during serialization").fmt(f)?;
        }

        Ok(())
    }
}

pub fn emit<'a>(codegen: &codegen::CodegenState<'a>) -> Node {
    let mut state = EmitState::new(codegen);
    emit_call(&mut state, "main", Vec::new(), None)
}

fn emit_call(
    state: &mut EmitState,
    function_name: &str,
    params: Vec<usize>,
    ret_val: Option<usize>,
) -> Node {
    // TODO check if intrinsic and simplify if appropriate (`+(const, const)`)
    // (alternatively preload the function table with the intrinsics (this will also prevent
    // overloading confusion))

    if let Some(node) = intrinsic(state, function_name, &params, ret_val) {
        return node;
    }

    let function_descr = state
        .codegen
        .functions
        .get(function_name)
        .unwrap_or_else(|| panic!("No function called {}", function_name));

    let parse::Item::FnDeclaration { name, params: declared_params, return_type, body } = function_descr.declaration else {
        panic!("Function representation contains non-function item");
    };

    // allocate return value as ""
    // TODO is this nescessary?
    // TODO if the type isn't empty
    //
    assert_eq!(params.len(), declared_params.len(), "Number of parameters in call doesn't match the declaration");
    let mut new_scope = HashMap::new();
    for (binding, name) in params.iter().zip(declared_params.iter().map(|param| param.name)) {
        let prev = new_scope
            .insert(name.to_owned(), *binding);
        if prev.is_some() {
            panic!("Two params are called {} in fn {}", name, function_name);
        }
    }
    state.var_scope.push(new_scope);

    // FIXME no function calls -- only inlining
    // state.functions is never set
    if let Some(mut function) = state.functions.get_mut(name.to_owned()) {
        if function.called {
            panic!("Recursion not yet implemented");
        }

        // FIXME set back to false at the end
        function.called = true;
    }

    let block = emit_block(state, body, ret_val);

    //for param in function.ast.
    //for statement in function.ast.block
    //


    /*
    state.var_scope.pop();
    todo!()
    */

    block
}

fn emit_block(state: &mut EmitState, code: &parse::Block, ret_val: Option<usize>) -> Node {
    let mut start = Node::empty();
    let current = &mut start;

    for statement in &code.code {
        let new = emit_statement(state, statement);
        current.append(new);
    }

    // FIXME emit return value if present
    
    match (ret_val, &code.value) {
        (Some(ret_val), Some(expr)) => {
            let new = emit_expr(state, expr, Some(ret_val));
            current.append(new);
            //current = current.children.last_mut().unwrap();
        },
        (None, None) => {

        },
        (Some(_), None) => {
            panic!("Return value not provided where requested");
        },
        (None, Some(_)) => {
            panic!("Return value provided where not requested");
        },
    }

    start
}

fn emit_statement(state: &mut EmitState, statement: &parse::Statement) -> Node {
    match statement {
        parse::Statement::Expr(expr) => emit_expr(state, expr, None),
        parse::Statement::StatementExpr(_) => todo!(),
        parse::Statement::Let { declaration, value } => emit_let(state, declaration, value),
        parse::Statement::Loop { body } => todo!(),
        parse::Statement::Break => todo!(),
    }
}

fn emit_let(
    state: &mut EmitState,
    declaration: &parse::VarDeclaration,
    value: &parse::Expr,
) -> Node {
    let parse::VarDeclaration { name, var_type } = declaration; // TODO don't ignore type
    let potential_binding = state.next_var();
    let binding = state
        .var_scope
        .last_mut()
        .unwrap_or_else(|| panic!("Let statement for {} outside any scope", name))
        .entry(name.to_string());
    match binding {
        Entry::Occupied(_) => {
            panic!("Double declaration of {}", name);
        }
        Entry::Vacant(vacant) => {
            vacant.insert(potential_binding);
        }
    }

    let ret_val = state.next_var();
    let mut compute_value = emit_expr(state, value, Some(ret_val));

    let set_value = Node::new(vec![Instruction::Set {
        target: Variable(potential_binding),
        value: Value::Variable(Variable(ret_val)),
    }]);

    compute_value.append(set_value);

    compute_value
}

fn emit_expr(state: &mut EmitState, expr: &parse::Expr, ret_val: Option<usize>) -> Node {
    match expr {
        parse::Expr::Tuple(_) => todo!(),
        parse::Expr::Constant(literal) => {
            if let Some(ret_val) = ret_val {
                emit_literal(state, literal, ret_val)
            } else {
                Node::empty()
            }
        }
        parse::Expr::Variable(variable) => {
            if let Some(ret_val) = ret_val {
                emit_variable(state, variable, ret_val)
            } else {
                Node::empty()
            }
        }
        parse::Expr::Application { name, params } => {
            // TODO change to asm! (currently ! is not allowed in functio-like calls)
            if name == &"asm" {
                return emit_asm(state, params);
            }

            let mut start = Node::empty();
            let mut current = &mut start;

            let mut param_bindings = Vec::new();
            for param in params {
                let binding = state.next_var();
                param_bindings.push(binding);
                let new = emit_expr(state, param, Some(binding));
                current.append(new);
            }

            let call = emit_call(state, name, param_bindings, ret_val);
            current.append(call);

            // FIXME better nodes
            if current.children.len() > 0 {
                panic!("{:?}", name);
            }

            start
        }
        parse::Expr::StatementExpr(_) => todo!(),
    }
}

fn emit_literal(state: &mut EmitState, literal: &parse::Literal, ret_val: usize) -> Node {
    let instruction = match literal {
        parse::Literal::Integer(str_repr) => {
            Instruction::Set { target: Variable(ret_val), value: Value::Number(str_repr.parse().expect("Either the literal isn't a valid number, or the parsing has to be implemented better")) }
        },
        parse::Literal::String(_) => todo!(),
    };

    Node {
        code: Block {
            code: vec![instruction],
        },
        children: Vec::new(),
    }
}

fn emit_variable(state: &mut EmitState, variable: &parse::Identifier, ret_val: usize) -> Node {
    let binding = find_binding(state, variable);
    let Some(binding) = binding else {
        panic!("Variable {} not in scope", variable);
    };
    let instruction = Instruction::Set {
        target: Variable(ret_val),
        value: Value::Variable(Variable(*binding)),
    };

    Node {
        code: Block {
            code: vec![instruction],
        },
        children: Vec::new(),
    }
}

fn find_binding<'a>(state: &'a EmitState, variable: &'a parse::Identifier) -> Option<&'a usize> {
    for scope in state.var_scope.iter().rev() {
        let binding = scope.get(*variable);
        if binding.is_some() {
            return binding;
        }
    }

    None
}

//fn builtin
// TODO move to file or something
fn intrinsic(
    state: &mut EmitState,
    function_name: &str,
    params: &Vec<usize>,
    ret_val: Option<usize>,
) -> Option<Node> {
    /*
    match function_name {
        //"asm!" => Some(emit_asm(state, function_name, params, ret_val)),
        "+" | "-" | "*" | "/" => {
            let lhs = params
                .get(0)
                .unwrap_or_else(|| panic!("\"{}\" has no lhs", function_name));
            let rhs = params
                .get(1)
                .unwrap_or_else(|| panic!("\"{}\" has no rhs", function_name));
            if let Some(ret_val) = ret_val {
                Some(Node::new(vec![Instruction::Operation {
                    target: ret_val,
                    operation: Operation::Add,
                    lhs: Value::Variable(*lhs),
                    rhs: Value::Variable(*rhs),
                }]))
            } else {
                Some(Node::empty())
            }
        }
        _ => None,
    }
    */
    None
}

// TODO enable emiting jumps
fn emit_asm(state: &mut EmitState, params: &Vec<parse::Expr>/*, ret_val: Option<usize>*/) -> Node {
    let mut inputs = Vec::new();
    let mut outputs = Vec::new();
    let mut instructions = Vec::new();

    for param in params {
        match param {
            parse::Expr::Application {
                name: "in",
                params, //} if let  = params[..] &&  => {
            } => match params.as_slice() {
                [parse::Expr::Constant(parse::Literal::String(identifier))]
                    if nom::combinator::all_consuming(parse::parse_identifier)(identifier)
                        .is_ok() => {
                    let identifier = &identifier.as_str();
                    let binding = find_binding(state, identifier);
                    let Some(binding) = binding else {
                        panic!("Variable {} not in scope while generating inline assembly", identifier);
                    };
                    inputs.push(Instruction::Raw(format!("set asm:{} var:{}", identifier, binding)))
                },
                _ => panic!("Invalid inline assembly input parameter")
            },
            parse::Expr::Application {
                name: "out",
                params,
            } => match params.as_slice() {
                [parse::Expr::Constant(parse::Literal::String(identifier))]
                    if nom::combinator::all_consuming(parse::parse_identifier)(identifier)
                        .is_ok() => {
                    let identifier = &identifier.as_str();
                    let binding = find_binding(state, identifier);
                    let Some(binding) = binding else {
                        panic!("Variable {} not in scope while generating inline assembly", identifier);
                    };
                    outputs.push(Instruction::Raw(format!("set var:{} asm:{}", binding, identifier)))
                },
                _ => panic!("Invalid inline assembly output parameter")
            },
            parse::Expr::Constant(parse::Literal::String(assembly)) => {
                instructions.push(Instruction::Raw(assembly.to_string()))
            },
            _ => panic!("Invalid inline assembly parameter")
        }
    }

    /*
    if let Some(ret_val) = ret_val {
        outputs.push(Instruction::Raw(format!("set var:{} ret", ret_val)))
    }
    */

    let mut inputs = Node::new(inputs);
    let instructions = Node::new(instructions);
    let outputs = Node::new(outputs);

    //instructions.append(outputs);
    inputs.append(instructions).append(outputs);
    
    inputs
}
