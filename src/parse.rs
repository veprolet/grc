use nom::bytes::complete::escaped_transform;
use nom::bytes::complete::is_not;
use nom::bytes::complete::take_until;
use nom::character::is_newline;
use nom::combinator::all_consuming;
use nom::combinator::not;
use nom::combinator::peek;
use nom::combinator::value;
use nom::combinator::verify;
use nom::multi::fold_many0;
use nom::multi::many0;
use nom::sequence::pair;
use nom::sequence::preceded;
use nom::sequence::separated_pair;
use nom::sequence::terminated;
use nom::AsChar;
use nom::{
    branch::alt,
    bytes::complete::{take_while, take_while1},
    character::complete::*,
    combinator::{map, opt, recognize},
    error::VerboseError,
    sequence::{delimited, tuple},
    IResult,
};
use nom::{bytes::complete::tag, multi::separated_list0};

// TODO figure out the indirections

#[derive(Debug, PartialEq, Clone)]
pub enum Literal<'a> {
    Integer(&'a str),
    String(String),
}

// Evaluates to concrete value
#[derive(Debug, PartialEq, Clone)]
pub enum Expr<'a> {
    Tuple(Vec<Expr<'a>>),
    Constant(Literal<'a>),
    Variable(Identifier<'a>),
    // function name, parameters
    Application {
        name: Identifier<'a>,
        params: Vec<Expr<'a>>,
    },
    StatementExpr(StatementExpr<'a>),
}

// Can be used as either expression statement depending on whether it returns ()
#[derive(Debug, PartialEq, Clone)]
pub enum StatementExpr<'a> {
    Block(Block<'a>),
    If(If<'a>),
}

#[derive(Debug, PartialEq, Clone)]
pub struct If<'a> {
    pub cond: Box<Expr<'a>>,
    pub then_body: Block<'a>,
    pub else_body: Option<Block<'a>>, // None => ()
}
#[derive(Debug, PartialEq, Clone)]
pub struct Block<'a> {
    pub code: Vec<Statement<'a>>,
    pub value: Option<Box<Expr<'a>>>, // None => ()
}
#[derive(Debug, PartialEq, Clone)]
pub struct Loop<'a> {
    pub body: Block<'a>,
    pub value: Option<Box<Expr<'a>>>, // None => ()
}

#[derive(Debug, PartialEq, Clone)]
pub enum Statement<'a> {
    Expr(Expr<'a>),
    StatementExpr(StatementExpr<'a>),
    Let {
        declaration: VarDeclaration<'a>,
        value: Expr<'a>,
    },
    Loop {
        body: Block<'a>,
    },
    Break,
}

#[derive(Debug, PartialEq, Clone)]
pub enum PrimitiveType {
    F64,
    Bool,
}

#[derive(Debug, PartialEq, Clone)]
pub enum Type {
    Primitive(PrimitiveType),
    Tuple(Vec<PrimitiveType>), // TODO parse tuple structs
}

#[derive(Debug, PartialEq, Clone)]
pub struct VarDeclaration<'a> {
    pub name: Identifier<'a>,
    pub var_type: Option<Type>,
}

#[derive(Debug, PartialEq, Clone)]
pub enum Item<'a> {
    FnDeclaration {
        name: Identifier<'a>,
        params: Vec<VarDeclaration<'a>>,
        return_type: Option<Type>,
        body: Block<'a>,
    },
}

#[derive(Debug, PartialEq, Clone)]
pub struct Module<'a> {
    pub name: Identifier<'a>,
    pub items: Vec<Item<'a>>,
}

pub type Identifier<'a> = &'a str;

// ignore surrounding whitespace and comments
fn ws<'a, F, O, E>(f: F) -> impl FnMut(&'a str) -> IResult<&'a str, O, E>
where
    F: FnMut(&'a str) -> IResult<&'a str, O, E>,
    //I: nom::InputTakeAtPosition + Clone + nom::Offset + nom::Slice<std::ops::RangeTo<usize>> + nom::InputLength,
    //<I as nom::InputTakeAtPosition>::Item: nom::AsChar + Clone,
    E: nom::error::ParseError<&'a str>,
{
    delimited(parse_ignored, f, parse_ignored)
}

pub fn parse_ignored<'a, E: nom::error::ParseError<&'a str>>(
    input: &'a str,
) -> IResult<&'a str, &'a str, E>
where
    //T: nom::InputTakeAtPosition + Clone + nom::Offset + nom::Slice<std::ops::RangeTo<usize>> + nom::InputLength,
    //<T as nom::InputTakeAtPosition>::Item: AsChar + Clone,
{
    recognize(many0(alt((multispace1, parse_comment))))(input)
}

fn parse_comment<'a, E: nom::error::ParseError<&'a str>>(
    input: &'a str,
) -> IResult<&'a str, &'a str, E> {
    // TODO This won't work with CRLF generally
    // TODO test EOF
    recognize(pair(
        tag("//"),
        /*is_not("\n\r")*/ take_while(|c| c != '\n'),
    ))(input)
}

// enclosed in parenthesis
fn paren<'a, F, O, E>(f: F) -> impl FnMut(&'a str) -> IResult<&'a str, O, E>
where
    F: FnMut(&'a str) -> IResult<&'a str, O, E>,
    //for<'b> E: nom::error::ParseError<&'b str>,
    E: nom::error::ParseError<&'a str>,
{
    delimited(ws(char('(')), f, ws(char(')')))
}

fn parse_integer(input: &str) -> IResult<&str, Literal, VerboseError<&str>> {
    ws(alt((
        /*
        map(tuple((opt(char('-')), one_of("123456789"), digit0)), |num: (Option<char>, char, &str)| {
            let sign = if num.0.is_some() { -1 } else { 1 };
            let first = num.1.to_string();
            let first = first.as_str();
            let number = [first, num.2].concat();
            Literal::Integer(sign * (number.parse::<i32>().unwrap()))
        }),
            */
        map(
            recognize(tuple((
                opt(char('-')),
                one_of("123456789"),
                many0(one_of("0123456789_")),
            ))),
            Literal::Integer,
        ),
        map(terminated(char('0'), peek(not(digit1))), |_| {
            Literal::Integer("0")
        }),
    )))(input)
}

fn parse_string(input: &str) -> IResult<&str, Literal, VerboseError<&str>> {
    map(
        ws(delimited(
            char('\"'),
            escaped_transform(
                none_of("\\\""),
                '\\',
                alt((value('\\', char('\\')), value('\"', char('\"')))),
            ),
            char('\"'),
        )),
        Literal::String,
    )(input)
}

fn parse_literal(input: &str) -> IResult<&str, Literal, VerboseError<&str>> {
    alt((parse_integer, parse_string))(input)
}

// identify special symbol (no brackets or quotes)
fn is_ascii_special(c: char) -> bool {
    matches!(
        c,
        '+' | '-' | '*' | '/' | '!' | '~' | '|' | '&' | '@' | '$' | '#' | '\\' | '<' | '>' | '='
    )
}
fn is_ascii_alphabetic(c: char) -> bool {
    c.is_ascii_alphabetic()
}
fn is_ascii_alphanumeric(c: char) -> bool {
    c.is_ascii_alphanumeric()
}

fn parse_tuple(input: &str) -> IResult<&str, Expr, VerboseError<&str>> {
    map(
        paren(terminated(
            separated_list0(ws(char(',')), parse_expr),
            opt(ws(char(','))),
        )),
        Expr::Tuple,
    )(input)
}

pub fn parse_identifier(input: &str) -> IResult<&str, Identifier, VerboseError<&str>> {
    recognize(tuple((
        satisfy(|c| is_ascii_alphabetic(c) || c == '_'),
        take_while(|c| is_ascii_alphanumeric(c) || c == '_'),
    )))(input)
}
fn parse_operator(input: &str) -> IResult<&str, Identifier, VerboseError<&str>> {
    take_while1(is_ascii_special)(input)
}
// all valid function names (infix and prefix)
fn parse_function_name(input: &str) -> IResult<&str, Identifier, VerboseError<&str>> {
    ws(alt((parse_identifier, parse_operator)))(input)
}

fn parse_constant(input: &str) -> IResult<&str, Expr, VerboseError<&str>> {
    map(parse_literal, Expr::Constant)(input)
}

fn parse_variable(input: &str) -> IResult<&str, Expr, VerboseError<&str>> {
    map(parse_identifier, Expr::Variable)(input)
}

fn parse_application(input: &str) -> IResult<&str, Expr, VerboseError<&str>> {
    map(
        tuple((
            parse_identifier,
            delimited(
                ws(char('(')),
                separated_list0(ws(char(',')), parse_expr),
                ws(char(')')),
            ),
        )),
        |(name, params)| Expr::Application { name, params },
    )(input)
}

// TODO monadic expresions
// parse expresion that is not made of infix application
fn parse_prefix(input: &str) -> IResult<&str, Expr, VerboseError<&str>> {
    alt((
        paren(parse_expr),
        parse_application,
        parse_constant,
        parse_variable,
    ))(input)
}

// right associativity
// TODO allow only operators?
fn parse_infix(input: &str) -> IResult<&str, Expr, VerboseError<&str>> {
    map(
        tuple((parse_prefix, parse_function_name, parse_expr)),
        |(lhs, op, rhs)| Expr::Application {
            name: op,
            params: vec![lhs, rhs],
        },
    )(input)
}

// left associativity
// *might* be better to write a dedicated parser for this
/*
fn parse_infix(input: &str) -> IResult<&str, Expr, VerboseError<&str>> {
    flat_map(parse_prefix, |init| {
        fold_many0(
            tuple((parse_function, parse_prefix)),
            init,
            |prefix, suffix| Expr::Application(suffix.0, vec![prefix, suffix.1]),
        )
    })(input)
}
*/

fn parse_block(input: &str) -> IResult<&str, Block, VerboseError<&str>> {
    map(
        delimited(
            ws(char('{')),
            tuple((many0(parse_statement), opt(parse_expr))),
            ws(char('}')),
        ),
        |(code, value)| Block {
            code,
            value: value.map(Box::new),
        },
    )(input)
}
// Parse a block that is an expression
fn parse_block_expr(input: &str) -> IResult<&str, Expr, VerboseError<&str>> {
    map(
        verify(parse_block, |block| block.value.is_some()),
        |block| Expr::StatementExpr(StatementExpr::Block(block)),
    )(input)
}

impl<'a> If<'a> {
    fn is_expr(&self) -> bool {
        let If {
            cond: _,
            then_body,
            else_body,
        } = self;

        // The if expression has to have both branches
        let Some(else_body) = else_body else {
                return false;
            };

        // Both branches have to return a value
        then_body.value.is_some() && else_body.value.is_some()
    }
}
fn parse_if(input: &str) -> IResult<&str, If, VerboseError<&str>> {
    map(
        preceded(
            ws(tag("if")),
            tuple((
                parse_expr,
                parse_block,
                opt(preceded(ws(tag("else")), parse_block)),
            )),
        ),
        |(cond, then_body, else_body)| If {
            cond: Box::new(cond),
            then_body,
            //else_body: Box::new(false_body.unwrap_or(Expr::Unit)),
            else_body,
        },
    )(input)
}
// Parse an if that is an expression
fn parse_if_expr(input: &str) -> IResult<&str, Expr, VerboseError<&str>> {
    map(verify(parse_if, |if_expr| if_expr.is_expr()), |if_expr| {
        Expr::StatementExpr(StatementExpr::If(if_expr))
    })(input)
}

// Parse something that is statement or expression based on context
//
// Do not confuse with `parse_expr_statement`
/*
fn parse_statement_expr(input: &str) -> IResult<&str, StatementExpr, VerboseError<&str>> {
    alt((parse_if, parse_block))(input)
}
*/

// Whitespace is handled in variants
fn parse_expr(input: &str) -> IResult<&str, Expr, VerboseError<&str>> {
    alt((
        parse_if_expr,
        parse_block_expr,
        parse_infix,
        parse_prefix,
        parse_tuple,
    ))(input)
}

// Parse expression in a statement context (ends in ';' and result is ignored)
//
// Do not confuse with `parse_statement_expr`
fn parse_expr_statement(input: &str) -> IResult<&str, Statement, VerboseError<&str>> {
    map(terminated(parse_expr, ws(char(';'))), Statement::Expr)(input)
}

fn parse_primitive_type(input: &str) -> IResult<&str, PrimitiveType, VerboseError<&str>> {
    alt((
        value(PrimitiveType::Bool, tag("bool")),
        value(PrimitiveType::F64, tag("f64")),
    ))(input)
}
fn parse_type(input: &str) -> IResult<&str, Type, VerboseError<&str>> {
    map(parse_primitive_type, Type::Primitive)(input)
}

fn parse_var_declaration(input: &str) -> IResult<&str, VarDeclaration, VerboseError<&str>> {
    map(
        pair(parse_identifier, opt(preceded(ws(char(':')), parse_type))),
        |(name, var_type)| VarDeclaration { name, var_type },
    )(input)
}

fn parse_let(input: &str) -> IResult<&str, Statement, VerboseError<&str>> {
    map(
        terminated(
            preceded(
                ws(tag("let")),
                separated_pair(parse_var_declaration, ws(char('=')), parse_expr),
            ),
            ws(char(';')),
        ),
        |(declaration, value)| Statement::Let { declaration, value },
    )(input)
}

// Parse an if that is a statement
fn parse_if_statement(input: &str) -> IResult<&str, Statement, VerboseError<&str>> {
    map(
        verify(parse_if, |if_statement| !if_statement.is_expr()),
        |if_statement| Statement::StatementExpr(StatementExpr::If(if_statement)),
    )(input)
}

fn parse_loop(input: &str) -> IResult<&str, Statement, VerboseError<&str>> {
    map(preceded(ws(tag("loop")), parse_block), |body| {
        Statement::Loop { body }
    })(input)
}
fn parse_break(input: &str) -> IResult<&str, Statement, VerboseError<&str>> {
    value(Statement::Break, pair(ws(tag("break")), ws(char(';'))))(input)
}

fn parse_statement(input: &str) -> IResult<&str, Statement, VerboseError<&str>> {
    alt((
        parse_let,
        parse_if_statement,
        parse_break,
        parse_expr_statement,
        parse_loop,
    ))(input)
}

fn parse_fn_declaration(input: &str) -> IResult<&str, Item, VerboseError<&str>> {
    map(
        preceded(
            ws(tag("fn")),
            tuple((
                parse_function_name,
                delimited(
                    ws(char('(')),
                    separated_list0(ws(char(',')), parse_var_declaration),
                    ws(char(')')),
                ),
                opt(preceded(ws(tag("->")), parse_type)),
                parse_block,
            )),
        ),
        |(name, params, return_type, body)| Item::FnDeclaration {
            name,
            params,
            return_type,
            body,
        },
    )(input)
}

fn parse_item(input: &str) -> IResult<&str, Item, VerboseError<&str>> {
    alt((parse_fn_declaration, parse_fn_declaration))(input)
}

pub fn parse_file_module<'a>(
    input: &'a str,
    filename: &'a str,
) -> IResult<&'a str, Module<'a>, VerboseError<&'a str>> {
    map(all_consuming(many0(parse_item)), |items| Module {
        name: filename,
        items,
    })(input)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn num_integer() {
        assert_eq!(
            parse_integer("48080 123"),
            Ok(("123", Literal::Integer("48080")))
        );
        assert_eq!(parse_integer("0+0"), Ok(("+0", Literal::Integer("0"))));
        assert_eq!(
            parse_integer("-1320 + 99"),
            Ok(("+ 99", Literal::Integer("-1320")))
        );
        assert_eq!(
            parse_integer("0-1320 + 99"),
            Ok(("-1320 + 99", Literal::Integer("0")))
        );

        assert!(parse_integer("-0").is_err());
        assert!(parse_integer("--0").is_err());
        assert!(parse_integer("00").is_err());
        assert!(parse_integer("0132").is_err());
    }

    #[test]
    fn function_names() {
        assert_eq!(parse_function_name("+ 1"), Ok(("1", "+")));
        assert_eq!(parse_function_name("foo(1)"), Ok(("(1)", "foo")));
    }

    #[test]
    fn expr() {
        assert_eq!(
            parse_expr("123"),
            Ok(("", Expr::Constant(Literal::Integer("123"))))
        );
        println!("another");
        assert_eq!(
            parse_expr("1+1"),
            Ok((
                "",
                Expr::Application {
                    name: "+",
                    params: vec![
                        Expr::Constant(Literal::Integer("1")),
                        Expr::Constant(Literal::Integer("1"))
                    ]
                }
            ))
        );
        assert_eq!(
            parse_expr("1 + 1"),
            Ok((
                "",
                Expr::Application {
                    name: "+",
                    params: vec![
                        Expr::Constant(Literal::Integer("1")),
                        Expr::Constant(Literal::Integer("1"))
                    ]
                }
            ))
        );

        /* assignment is not a function
        assert_eq!(
            parse_expr("1 += 1"),
            Ok(("+= 1", Expr::Constant(Literal::Integer(1)),))
        );
        */

        // assignment is a function
        assert_eq!(
            parse_expr("1 += 1"),
            Ok((
                "",
                Expr::Application {
                    name: "+=",
                    params: vec![
                        Expr::Constant(Literal::Integer("1")),
                        Expr::Constant(Literal::Integer("1"))
                    ]
                }
            ))
        );

        // right associativity
        assert_eq!(
            parse_expr("8 / 2 + 2"),
            Ok((
                "",
                Expr::Application {
                    name: "/",
                    params: vec![
                        Expr::Constant(Literal::Integer("8")),
                        Expr::Application {
                            name: "+",
                            params: vec![
                                Expr::Constant(Literal::Integer("2")),
                                Expr::Constant(Literal::Integer("2"))
                            ]
                        }
                    ]
                }
            ))
        );

        assert_eq!(
            parse_expr("7 + a"),
            Ok((
                "",
                Expr::Application {
                    name: "+",
                    params: vec![Expr::Constant(Literal::Integer("7")), Expr::Variable("a")]
                }
            ))
        );
        assert_eq!(
            parse_expr("a + 7"),
            Ok((
                "",
                Expr::Application {
                    name: "+",
                    params: vec![Expr::Variable("a"), Expr::Constant(Literal::Integer("7"))]
                }
            ))
        );
    }
    #[test]
    fn expr_parentheses() {
        assert_eq!(
            parse_expr("(8 / 2) + 2"),
            Ok((
                "",
                Expr::Application {
                    name: "+",
                    params: vec![
                        Expr::Application {
                            name: "/",
                            params: vec![
                                Expr::Constant(Literal::Integer("8")),
                                Expr::Constant(Literal::Integer("2"))
                            ]
                        },
                        Expr::Constant(Literal::Integer("2"))
                    ]
                }
            ))
        );
        assert_eq!(
            parse_expr("8 / (2 + 2)"),
            Ok((
                "",
                Expr::Application {
                    name: "/",
                    params: vec![
                        Expr::Constant(Literal::Integer("8")),
                        Expr::Application {
                            name: "+",
                            params: vec![
                                Expr::Constant(Literal::Integer("2")),
                                Expr::Constant(Literal::Integer("2"))
                            ]
                        }
                    ]
                }
            ))
        );
        assert_eq!(
            parse_expr("5 + 4 - 3 * 2 / 1"),
            Ok((
                "",
                Expr::Application {
                    name: "+",
                    params: vec![
                        Expr::Constant(Literal::Integer("5")),
                        Expr::Application {
                            name: "-",
                            params: vec![
                                Expr::Constant(Literal::Integer("4")),
                                Expr::Application {
                                    name: "*",
                                    params: vec![
                                        Expr::Constant(Literal::Integer("3")),
                                        Expr::Application {
                                            name: "/",
                                            params: vec![
                                                Expr::Constant(Literal::Integer("2")),
                                                Expr::Constant(Literal::Integer("1"))
                                            ]
                                        }
                                    ]
                                }
                            ]
                        }
                    ]
                }
            ))
        );
    }
    #[test]
    fn expr_if() {
        assert_eq!(
            parse_expr("if a == 5 { 5 } else { 0 }"),
            Ok((
                "",
                Expr::StatementExpr(StatementExpr::If(If {
                    cond: Box::new(Expr::Application {
                        name: "==",
                        params: vec![Expr::Variable("a"), Expr::Constant(Literal::Integer("5"))]
                    }),
                    then_body: Block {
                        code: vec![],
                        value: Some(Box::new(Expr::Constant(Literal::Integer("5")))),
                    },
                    else_body: Some(Block {
                        code: vec![],
                        value: Some(Box::new(Expr::Constant(Literal::Integer("0"))))
                    }),
                }))
            ))
        );
    }
    #[test]
    fn expr_tuple() {
        assert_eq!(
            parse_expr("(1, 2, 3)"),
            Ok((
                "",
                Expr::Tuple(vec![
                    Expr::Constant(Literal::Integer("1")),
                    Expr::Constant(Literal::Integer("2")),
                    Expr::Constant(Literal::Integer("3"))
                ])
            ))
        );
        assert_eq!(
            parse_expr("(1,)"),
            Ok((
                "",
                Expr::Tuple(vec![Expr::Constant(Literal::Integer("1")),])
            ))
        );
        assert_eq!(parse_expr("()"), Ok(("", Expr::Tuple(vec![]))));
    }
    #[test]
    fn fn_declaration() {
        assert_eq!(
            parse_item("fn main() {}"),
            Ok((
                "",
                Item::FnDeclaration {
                    name: "main",
                    params: vec![],
                    return_type: None,
                    body: Block {
                        code: vec![],
                        value: None
                    }
                }
            ))
        );
        assert_eq!(
            parse_item("fn foo_foo(bar: f64) -> bool { count(123) }"),
            Ok((
                "",
                Item::FnDeclaration {
                    name: "foo_foo",
                    params: vec![VarDeclaration {
                        name: "bar",
                        var_type: Some(Type::Primitive(PrimitiveType::F64))
                    }],
                    return_type: Some(Type::Primitive(PrimitiveType::Bool)),
                    body: Block {
                        code: vec![],
                        value: Some(Box::new(Expr::Application {
                            name: "count",
                            params: vec![Expr::Constant(Literal::Integer("123"))]
                        }))
                    }
                }
            ))
        );
        assert_eq!(
            parse_item("fn foo(self, doubt: f64) {}"),
            Ok((
                "",
                Item::FnDeclaration {
                    name: "foo",
                    params: vec![VarDeclaration {
                        name: "self",
                        var_type: None
                    }, VarDeclaration {
                        name: "doubt",
                        var_type: Some(Type::Primitive(PrimitiveType::F64))
                    }],
                    return_type: None,
                    body: Block {
                        code: vec![],
                        value: None
                    }
                }
            ))
        );
    }
    #[test]
    fn file_module() {
        assert_eq!(
            parse_file_module(r#"fn foo() {} fn bar() {}"#, "baz"),
            Ok((
                "",
                Module {
                    name: "baz",
                    items: vec![
                        Item::FnDeclaration {
                        name: "foo",
                        params: vec![],
                        return_type: None,
                        body: Block {
                            code: vec![],
                            value: None
                        }
                    },
                        Item::FnDeclaration {
                        name: "bar",
                        params: vec![],
                        return_type: None,
                        body: Block {
                            code: vec![],
                            value: None
                        }
                    },
                    ]
                }
            ))
        );
    }
    #[test]
    fn string() {
        assert_eq!(
            parse_expr(r#"msg = "hello mr. \"president\" \\s""#),
            Ok((
                "",
                Expr::Application {
                    name: "=",
                    params: vec![
                        Expr::Variable("msg"),
                        Expr::Constant(Literal::String(r#"hello mr. "president" \s"#.to_owned()))
                    ]
                }
            ))
        );
    }
    #[test]
    fn loops() {
        assert_eq!(
            parse_statement(r#"loop { break; }"#),
            Ok((
                "",
                Statement::Loop {
                    body: Block {
                        code: vec![Statement::Break],
                        value: None
                    }
                }
            ))
        );
    }
    #[test]
    fn ignored_sequences() {
        assert_eq!(
            parse_comment::<nom::error::VerboseError<&str>>("//short"),
            Ok(("", "//short"))
        );
        assert_eq!(
            parse_ignored::<nom::error::VerboseError<&str>>(" //spacious"),
            Ok(("", " //spacious"))
        );
        assert_eq!(
            parse_ignored::<nom::error::VerboseError<&str>>(
                r#"
//multiline
hello"#
            ),
            Ok((
                "hello",
                r#"
//multiline
"#
            ))
        );
        assert_eq!(
            ws(parse_identifier)(
                r#"
                // an appropriate comment
                //
                hello

                // another one
                "#
            ),
            Ok(("", "hello"))
        );
    }
}
