#![feature(let_else)]

use std::fs::read_to_string;

mod parse;
mod codegen;
mod emit;

fn main() {
    let code = read_to_string("test_code").expect("Invalid file");
    let ast = parse::parse_file_module(code.as_str(), "main").expect("Parsing failed").1;
    //println!("{:#?}", &ast);
    let mut state = codegen::CodegenState::new();
    codegen::codegen_module(&mut state, &ast);
    let dfg = emit::emit(&state);
    println!("{}", &dfg);
    //emit(ast);
}
